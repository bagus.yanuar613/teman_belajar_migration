<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201127144637 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE tb_account CHANGE type type SMALLINT NOT NULL COMMENT \'tipe akun: 0: admin, 1: tentor, 2: orangtua, 3: murid\'');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE tb_account CHANGE type type TINYINT(1) NOT NULL COMMENT \'tipe akun: 0: admin, 1: tentor, 2: orangtua, 3: murid\'');
    }
}
