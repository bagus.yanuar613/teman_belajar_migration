<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201127142710 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE tb_subject_school_level CHANGE subject_id subject_id INT DEFAULT NULL, CHANGE school_level_id school_level_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE tb_subjects CHANGE subject_category_id subject_category_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE tb_tentor_keahlian ADD has_parent TINYINT(1) NOT NULL COMMENT \'1: active, 0: not_active\', CHANGE subjectschoollevel_id subjectschoollevel_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE tb_tentor_profile CHANGE account_id account_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE tb_user_profile CHANGE account_id account_id INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE tb_subject_school_level CHANGE school_level_id school_level_id INT NOT NULL, CHANGE subject_id subject_id INT NOT NULL');
        $this->addSql('ALTER TABLE tb_subjects CHANGE subject_category_id subject_category_id INT NOT NULL');
        $this->addSql('ALTER TABLE tb_tentor_keahlian DROP has_parent, CHANGE subjectschoollevel_id subjectschoollevel_id INT NOT NULL');
        $this->addSql('ALTER TABLE tb_tentor_profile CHANGE account_id account_id INT NOT NULL');
        $this->addSql('ALTER TABLE tb_user_profile CHANGE account_id account_id INT NOT NULL');
    }
}
