<?php

namespace App\Repository;

use App\Entity\TbSubjectSchoolLevel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TbSubjectSchoolLevel|null find($id, $lockMode = null, $lockVersion = null)
 * @method TbSubjectSchoolLevel|null findOneBy(array $criteria, array $orderBy = null)
 * @method TbSubjectSchoolLevel[]    findAll()
 * @method TbSubjectSchoolLevel[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TbSubjectSchoolLevelRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TbSubjectSchoolLevel::class);
    }

    // /**
    //  * @return TbSubjectSchoolLevel[] Returns an array of TbSubjectSchoolLevel objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TbSubjectSchoolLevel
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
