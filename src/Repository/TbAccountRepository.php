<?php

namespace App\Repository;

use App\Entity\TbAccount;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TbAccount|null find($id, $lockMode = null, $lockVersion = null)
 * @method TbAccount|null findOneBy(array $criteria, array $orderBy = null)
 * @method TbAccount[]    findAll()
 * @method TbAccount[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TbAccountRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TbAccount::class);
    }

    // /**
    //  * @return TbAccount[] Returns an array of TbAccount objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TbAccount
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
