<?php

namespace App\Repository;

use App\Entity\TbTentorProfile;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TbTentorProfile|null find($id, $lockMode = null, $lockVersion = null)
 * @method TbTentorProfile|null findOneBy(array $criteria, array $orderBy = null)
 * @method TbTentorProfile[]    findAll()
 * @method TbTentorProfile[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TbTentorProfileRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TbTentorProfile::class);
    }

    // /**
    //  * @return TbTentorProfile[] Returns an array of TbTentorProfile objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TbTentorProfile
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
