<?php

namespace App\Repository;

use App\Entity\TbUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TbUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method TbUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method TbUser[]    findAll()
 * @method TbUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TbUserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TbUser::class);
    }

    // /**
    //  * @return TbUser[] Returns an array of TbUser objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TbUser
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
