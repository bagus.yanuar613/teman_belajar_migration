<?php

namespace App\Repository;

use App\Entity\TbTentorKeahlian;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TbTentorKeahlian|null find($id, $lockMode = null, $lockVersion = null)
 * @method TbTentorKeahlian|null findOneBy(array $criteria, array $orderBy = null)
 * @method TbTentorKeahlian[]    findAll()
 * @method TbTentorKeahlian[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TbTentorKeahlianRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TbTentorKeahlian::class);
    }

    // /**
    //  * @return TbTentorKeahlian[] Returns an array of TbTentorKeahlian objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TbTentorKeahlian
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
