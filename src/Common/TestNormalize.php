<?php


namespace App\Common;


use App\Entity\TentorSubject;
use Symfony\Component\Serializer\Exception\CircularReferenceException;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Exception\InvalidArgumentException;
use Symfony\Component\Serializer\Exception\LogicException;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class TestNormalize implements NormalizerInterface
{

    /**
     * @inheritDoc
     */

    public function normalize($object, string $format = null, array $context = [])
    {
        // TODO: Implement normalize() method.
        return [
            'id' => $object->getId(),
            'tentor' => $object->getUser()->getTentorProfile()->getFullName(),
            'about' => $object->getUser()->getTentorProfile()->getAbout(),
            'subject' => $object->getSubject()->getName()
        ];
    }

    /**
     * @inheritDoc
     */
    public function supportsNormalization($data, string $format = null)
    {
        // TODO: Implement supportsNormalization() method.
        return $data instanceof TentorSubject;
    }
}