<?php


namespace App\Controller;


use App\Entity\TbAccount;
use App\Entity\TbTentorKeahlian;
use App\Entity\TbTentorProfile;
use App\Entity\TbUserProfile;
use App\Repository\TbAccountRepository;
use App\Repository\TbTentorKeahlianRepository;
use App\Repository\TbTentorProfileRepository;
use App\Repository\TbUserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use App\Common\GenBasic;

class UserController extends BaseController
{

    /**
     * UserController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->req = Request::createFromGlobals();
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/user", methods="GET|HEAD")
     */
    public function getUserInformation()
    {
        try {
            $page = 5;
            $limit = 100;
            $offset = ($page * $limit);
            /** @var TbAccountRepository $userRepo */
            $userRepo = $this->em->getRepository(TbAccount::class);
            $user = $userRepo->createQueryBuilder('u')
                ->where('u.type = 2')
                ->orWhere('u.type = 3')
                ->orderBy('u.id', 'ASC')
                ->setFirstResult($offset)
                ->setMaxResults($limit)
                ->groupBy('u.email')
                ->getQuery()
                ->getResult();
            $this->em->flush();
            $code = 200;
            $response = GenBasic::CustomNormalize($user, [
                'id',
                'type',
                'email',
                'phone',
                'password',
                'status',
                'source',
                'ava',
                'memberProfile' => [
                    'id',
                    'fullname',
                    'permalink',
                    'phone',
                    'school',
                    'class',
                    'fullAddress',
                    'status',
                ]
            ]);
        }catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Check Attendance ' . $err->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize Attendance ' . $e->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }
    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/tentor", methods="GET|HEAD")
     */
    public function getTentorInformation()
    {
        try {
            $page = $this->req->query->get('page');
            $limit = 10;
            $offset = ($page * $limit);
            /** @var TbAccountRepository $userRepo */
            $userRepo = $this->em->getRepository(TbAccount::class);
            $user = $userRepo->createQueryBuilder('u')
                ->where('u.type = 1')
                ->orderBy('u.id', 'ASC')
                ->setFirstResult($offset)
                ->setMaxResults($limit)
                ->getQuery()
                ->getResult();
            $this->em->flush();
            $code = 200;
            $response = GenBasic::CustomNormalize($user, [
                'id',
                'type',
                'email',
                'phone',
                'password',
                'status',
                'source',
                'ava',
                'tentorProfile' => [
                    'id',
                    'fullname',
                    'permalink',
                    'aboutMe',
                    'experience',
                    'education',
                    'achievement',
                    'workAt',
                    'workAt',
                ]
            ]);
        }catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Check Attendance ' . $err->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize Attendance ' . $e->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/cek-parent-user", methods="GET|HEAD")
     */
    public function checkHasParent()
    {
        try {
            /** @var TbUserRepository $profileRepo */
            $profileRepo = $this->em->getRepository(TbUserProfile::class);
            $profile = $profileRepo->createQueryBuilder('p')
                ->orderBy('p.account', 'ASC')
                ->getQuery()
                ->getResult();
            /** @var TbUserProfile $v */
            foreach ($profile as $v){
                $accountId = $v->getAccount();
                $accountUser = $this->em->getRepository(TbAccount::class)->findOneBy(['id' => $accountId]);
                if($accountUser){
                   $v->setHasParent(true);
                }
            }
            $this->em->flush();
            $code = 200;
            $response = 'success';
        }catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Check Attendance ' . $err->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize Attendance ' . $e->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/cek-parent-keahlian", methods="GET|HEAD")
     */
    public function checkHasParentKeahlian()
    {
        try {
            /** @var TbTentorKeahlianRepository $keahlianRepo */
            $keahlianRepo = $this->em->getRepository(TbTentorKeahlian::class);
//            $keahlian = $keahlianRepo->createQueryBuilder('k')
//                ->orderBy('k.id', 'ASC')
//                ->getQuery()
//                ->getResult();


//            /** @var TbTentorKeahlian $v */
//            foreach ($keahlian as $v){
//                $accountId = $v->getTentorId();
//                $accountUser = $this->em->getRepository(TbTentorProfile::class)->findOneBy(['id' => $accountId]);
//                if($accountUser){
//                    $v->setHasParent(true);
//                }
//            }
//            $this->em->flush();

            $keahlian = $keahlianRepo->createQueryBuilder('k')
                ->orderBy('k.id', 'ASC')
                ->where('k.hasParent = false')
                ->getQuery()
                ->getResult();
            $id = [];
            /** @var TbTentorKeahlian $v */
            foreach ($keahlian as $v){
                array_push($id, $v->getTentorId());
            }
            $user = $this->em->getRepository(TbAccount::class)->findBy(['id' => $id]);
            $response = GenBasic::CustomNormalize($user, [
                'id',
                'type'
            ]);
            $code = 200;
//            $response = 'success';
        }catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Check Attendance ' . $err->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize Attendance ' . $e->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }
    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/cek-parent-tentor", methods="GET|HEAD")
     */
    public function checkTentorHasParent()
    {
        try {
            /** @var TbTentorProfileRepository $profileRepo */
            $profileRepo = $this->em->getRepository(TbTentorProfile::class);
            $profile = $profileRepo->createQueryBuilder('p')
                ->orderBy('p.accountId', 'ASC')
                ->getQuery()
                ->getResult();
            /** @var TbTentorProfile $v */
            foreach ($profile as $v){
                $accountId = $v->getAccountId();
                $accountUser = $this->em->getRepository(TbAccount::class)->findOneBy(['id' => $accountId]);
                if($accountUser){
                    $v->setHasParent(true);
                }
            }
            $this->em->flush();
            $code = 200;
            $response = 'success';
        }catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Check Attendance ' . $err->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize Attendance ' . $e->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/keahlian", methods="GET|HEAD")
     */
    public function tentorKeahlian()
    {
        try {
            /** @var TbTentorKeahlianRepository $keahlianRepo */
            $keahlianRepo = $this->em->getRepository(TbTentorKeahlian::class);
            $keahlian = $keahlianRepo->createQueryBuilder('k')
                ->setFirstResult(0)
                ->setMaxResults(10)
                ->orderBy('k.id', 'ASC')
                ->getQuery()
                ->getResult();
            $code = 200;
            $response = GenBasic::CustomNormalize($keahlian, [
                'id',
                'tentor' => [
                    'id', 'fullname'
                ],
                'subjectschoollevel' => [
                    'schoolLevel' => [
                        'id', 'name'
                    ],
                    'subject' => [
                        'id', 'subjectName'
                    ]
                ]
            ]);
        }catch (\Exception $err) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Check Attendance ' . $err->getMessage(),
            ];
        } catch (ExceptionInterface $e) {
            $code = 500;
            $response = [
                'msg' => 'Failed To Normalize Attendance ' . $e->getMessage(),
            ];
        }
        return GenBasic::send($code, $response);
    }
}