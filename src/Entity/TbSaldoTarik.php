<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TbSaldoTarik
 *
 * @ORM\Table(name="tb_saldo_tarik")
 * @ORM\Entity
 */
class TbSaldoTarik
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="type", type="boolean", nullable=false, options={"comment"="0: user, 1: tentor"})
     */
    private $type;

    /**
     * @var int
     *
     * @ORM\Column(name="pemohon_id", type="integer", nullable=false)
     */
    private $pemohonId;

    /**
     * @var int
     *
     * @ORM\Column(name="bank_id", type="integer", nullable=false)
     */
    private $bankId;

    /**
     * @var string
     *
     * @ORM\Column(name="nomor_rekening", type="string", length=50, nullable=false)
     */
    private $nomorRekening;

    /**
     * @var string
     *
     * @ORM\Column(name="atas_nama", type="string", length=75, nullable=false)
     */
    private $atasNama;

    /**
     * @var int
     *
     * @ORM\Column(name="jml_penarikan", type="integer", nullable=false)
     */
    private $jmlPenarikan;

    /**
     * @var bool
     *
     * @ORM\Column(name="status", type="boolean", nullable=false, options={"comment"="0: baru, 1: diterima"})
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="tgl_pengajuan", type="datetime", nullable=false)
     */
    private $tglPengajuan;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="tgl_konfirmasi", type="datetime", nullable=true)
     */
    private $tglKonfirmasi;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?bool
    {
        return $this->type;
    }

    public function setType(bool $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getPemohonId(): ?int
    {
        return $this->pemohonId;
    }

    public function setPemohonId(int $pemohonId): self
    {
        $this->pemohonId = $pemohonId;

        return $this;
    }

    public function getBankId(): ?int
    {
        return $this->bankId;
    }

    public function setBankId(int $bankId): self
    {
        $this->bankId = $bankId;

        return $this;
    }

    public function getNomorRekening(): ?string
    {
        return $this->nomorRekening;
    }

    public function setNomorRekening(string $nomorRekening): self
    {
        $this->nomorRekening = $nomorRekening;

        return $this;
    }

    public function getAtasNama(): ?string
    {
        return $this->atasNama;
    }

    public function setAtasNama(string $atasNama): self
    {
        $this->atasNama = $atasNama;

        return $this;
    }

    public function getJmlPenarikan(): ?int
    {
        return $this->jmlPenarikan;
    }

    public function setJmlPenarikan(int $jmlPenarikan): self
    {
        $this->jmlPenarikan = $jmlPenarikan;

        return $this;
    }

    public function getStatus(): ?bool
    {
        return $this->status;
    }

    public function setStatus(bool $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getTglPengajuan(): ?\DateTimeInterface
    {
        return $this->tglPengajuan;
    }

    public function setTglPengajuan(\DateTimeInterface $tglPengajuan): self
    {
        $this->tglPengajuan = $tglPengajuan;

        return $this;
    }

    public function getTglKonfirmasi(): ?\DateTimeInterface
    {
        return $this->tglKonfirmasi;
    }

    public function setTglKonfirmasi(?\DateTimeInterface $tglKonfirmasi): self
    {
        $this->tglKonfirmasi = $tglKonfirmasi;

        return $this;
    }


}
