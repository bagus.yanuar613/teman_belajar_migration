<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TbOrder
 *
 * @ORM\Table(name="tb_order", uniqueConstraints={@ORM\UniqueConstraint(name="trx_code", columns={"trx_code"})})
 * @ORM\Entity
 */
class TbOrder
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="trx_code", type="string", length=15, nullable=false)
     */
    private $trxCode;

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    private $userId;

    /**
     * @var int
     *
     * @ORM\Column(name="tentor_id", type="integer", nullable=false)
     */
    private $tentorId;

    /**
     * @var int
     *
     * @ORM\Column(name="subject_id", type="integer", nullable=false)
     */
    private $subjectId;

    /**
     * @var int
     *
     * @ORM\Column(name="school_level_id", type="integer", nullable=false)
     */
    private $schoolLevelId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="coupon_id", type="integer", nullable=true)
     */
    private $couponId;

    /**
     * @var bool
     *
     * @ORM\Column(name="jml_murid", type="boolean", nullable=false)
     */
    private $jmlMurid;

    /**
     * @var bool
     *
     * @ORM\Column(name="jml_pertemuan", type="boolean", nullable=false)
     */
    private $jmlPertemuan;

    /**
     * @var int
     *
     * @ORM\Column(name="total_diskon", type="integer", nullable=false)
     */
    private $totalDiskon;

    /**
     * @var int
     *
     * @ORM\Column(name="kode_unik", type="integer", nullable=false)
     */
    private $kodeUnik;

    /**
     * @var int
     *
     * @ORM\Column(name="total_harga", type="integer", nullable=false)
     */
    private $totalHarga;

    /**
     * @var string|null
     *
     * @ORM\Column(name="catatan", type="text", length=65535, nullable=true)
     */
    private $catatan;

    /**
     * @var bool
     *
     * @ORM\Column(name="durasi_les", type="boolean", nullable=false, options={"comment"="hitungan dalam menit"})
     */
    private $durasiLes;

    /**
     * @var bool
     *
     * @ORM\Column(name="status_pembayaran", type="boolean", nullable=false, options={"comment"="0: belum, 1: sudah"})
     */
    private $statusPembayaran;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="status_les", type="boolean", nullable=true, options={"comment"="0: baru, 1: ditolak tentor, 2: murid berhenti, 3: aktif"})
     */
    private $statusLes;

    /**
     * @var string|null
     *
     * @ORM\Column(name="alasan", type="text", length=65535, nullable=true, options={"comment"="diisi hanya jika status les 1 dan 2"})
     */
    private $alasan;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="order_time", type="datetime", nullable=false)
     */
    private $orderTime;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="tgl_tentor_konfirmasi", type="datetime", nullable=true)
     */
    private $tglTentorKonfirmasi;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="tgl_admin_konfirmasi", type="datetime", nullable=true)
     */
    private $tglAdminKonfirmasi;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified_at", type="datetime", nullable=false)
     */
    private $modifiedAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTrxCode(): ?string
    {
        return $this->trxCode;
    }

    public function setTrxCode(string $trxCode): self
    {
        $this->trxCode = $trxCode;

        return $this;
    }

    public function getUserId(): ?int
    {
        return $this->userId;
    }

    public function setUserId(int $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    public function getTentorId(): ?int
    {
        return $this->tentorId;
    }

    public function setTentorId(int $tentorId): self
    {
        $this->tentorId = $tentorId;

        return $this;
    }

    public function getSubjectId(): ?int
    {
        return $this->subjectId;
    }

    public function setSubjectId(int $subjectId): self
    {
        $this->subjectId = $subjectId;

        return $this;
    }

    public function getSchoolLevelId(): ?int
    {
        return $this->schoolLevelId;
    }

    public function setSchoolLevelId(int $schoolLevelId): self
    {
        $this->schoolLevelId = $schoolLevelId;

        return $this;
    }

    public function getCouponId(): ?int
    {
        return $this->couponId;
    }

    public function setCouponId(?int $couponId): self
    {
        $this->couponId = $couponId;

        return $this;
    }

    public function getJmlMurid(): ?bool
    {
        return $this->jmlMurid;
    }

    public function setJmlMurid(bool $jmlMurid): self
    {
        $this->jmlMurid = $jmlMurid;

        return $this;
    }

    public function getJmlPertemuan(): ?bool
    {
        return $this->jmlPertemuan;
    }

    public function setJmlPertemuan(bool $jmlPertemuan): self
    {
        $this->jmlPertemuan = $jmlPertemuan;

        return $this;
    }

    public function getTotalDiskon(): ?int
    {
        return $this->totalDiskon;
    }

    public function setTotalDiskon(int $totalDiskon): self
    {
        $this->totalDiskon = $totalDiskon;

        return $this;
    }

    public function getKodeUnik(): ?int
    {
        return $this->kodeUnik;
    }

    public function setKodeUnik(int $kodeUnik): self
    {
        $this->kodeUnik = $kodeUnik;

        return $this;
    }

    public function getTotalHarga(): ?int
    {
        return $this->totalHarga;
    }

    public function setTotalHarga(int $totalHarga): self
    {
        $this->totalHarga = $totalHarga;

        return $this;
    }

    public function getCatatan(): ?string
    {
        return $this->catatan;
    }

    public function setCatatan(?string $catatan): self
    {
        $this->catatan = $catatan;

        return $this;
    }

    public function getDurasiLes(): ?bool
    {
        return $this->durasiLes;
    }

    public function setDurasiLes(bool $durasiLes): self
    {
        $this->durasiLes = $durasiLes;

        return $this;
    }

    public function getStatusPembayaran(): ?bool
    {
        return $this->statusPembayaran;
    }

    public function setStatusPembayaran(bool $statusPembayaran): self
    {
        $this->statusPembayaran = $statusPembayaran;

        return $this;
    }

    public function getStatusLes(): ?bool
    {
        return $this->statusLes;
    }

    public function setStatusLes(?bool $statusLes): self
    {
        $this->statusLes = $statusLes;

        return $this;
    }

    public function getAlasan(): ?string
    {
        return $this->alasan;
    }

    public function setAlasan(?string $alasan): self
    {
        $this->alasan = $alasan;

        return $this;
    }

    public function getOrderTime(): ?\DateTimeInterface
    {
        return $this->orderTime;
    }

    public function setOrderTime(\DateTimeInterface $orderTime): self
    {
        $this->orderTime = $orderTime;

        return $this;
    }

    public function getTglTentorKonfirmasi(): ?\DateTimeInterface
    {
        return $this->tglTentorKonfirmasi;
    }

    public function setTglTentorKonfirmasi(?\DateTimeInterface $tglTentorKonfirmasi): self
    {
        $this->tglTentorKonfirmasi = $tglTentorKonfirmasi;

        return $this;
    }

    public function getTglAdminKonfirmasi(): ?\DateTimeInterface
    {
        return $this->tglAdminKonfirmasi;
    }

    public function setTglAdminKonfirmasi(?\DateTimeInterface $tglAdminKonfirmasi): self
    {
        $this->tglAdminKonfirmasi = $tglAdminKonfirmasi;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getModifiedAt(): ?\DateTimeInterface
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt(\DateTimeInterface $modifiedAt): self
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }


}
