<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TbTentorJadwal
 *
 * @ORM\Table(name="tb_tentor_jadwal")
 * @ORM\Entity
 */
class TbTentorJadwal
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="tentor_id", type="integer", nullable=false)
     */
    private $tentorId;

    /**
     * @var string
     *
     * @ORM\Column(name="hari", type="string", length=10, nullable=false)
     */
    private $hari;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="jam", type="time", nullable=false)
     */
    private $jam;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTentorId(): ?int
    {
        return $this->tentorId;
    }

    public function setTentorId(int $tentorId): self
    {
        $this->tentorId = $tentorId;

        return $this;
    }

    public function getHari(): ?string
    {
        return $this->hari;
    }

    public function setHari(string $hari): self
    {
        $this->hari = $hari;

        return $this;
    }

    public function getJam(): ?\DateTimeInterface
    {
        return $this->jam;
    }

    public function setJam(\DateTimeInterface $jam): self
    {
        $this->jam = $jam;

        return $this;
    }


}
