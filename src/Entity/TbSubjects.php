<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * TbSubjects
 *
 * @ORM\Table(name="tb_subjects", indexes={@ORM\Index(name="subject_category_id", columns={"subject_category_id"})})
 * @ORM\Entity(repositoryClass="App\Repository\TbSubjectsRepository")
 */
class TbSubjects
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=4, nullable=false)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="subject_name", type="string", length=255, nullable=false)
     */
    private $subjectName;

    /**
     * @var string
     *
     * @ORM\Column(name="permalink", type="string", length=255, nullable=false)
     */
    private $permalink;

    /**
     * @var string|null
     *
     * @ORM\Column(name="icon_class_or_url", type="text", length=65535, nullable=true, options={"comment"="font awesome class or asset url"})
     */
    private $iconClassOrUrl;

    /**
     * @var string|null
     *
     * @ORM\Column(name="icon_color_code", type="string", length=10, nullable=true, options={"comment"="icon color hex code"})
     */
    private $iconColorCode;

    /**
     * @var \TbSubjectCategory
     *
     * @ORM\ManyToOne(targetEntity="TbSubjectCategory")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="subject_category_id", referencedColumnName="id")
     * })
     */
    private $subjectCategory;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TbSubjectSchoolLevel", mappedBy="subject")
     */
    private $subjectSchoolLevel;

    public function __construct()
    {
        $this->subjectSchoolLevel = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getSubjectName(): ?string
    {
        return $this->subjectName;
    }

    public function setSubjectName(string $subjectName): self
    {
        $this->subjectName = $subjectName;

        return $this;
    }

    public function getPermalink(): ?string
    {
        return $this->permalink;
    }

    public function setPermalink(string $permalink): self
    {
        $this->permalink = $permalink;

        return $this;
    }

    public function getIconClassOrUrl(): ?string
    {
        return $this->iconClassOrUrl;
    }

    public function setIconClassOrUrl(?string $iconClassOrUrl): self
    {
        $this->iconClassOrUrl = $iconClassOrUrl;

        return $this;
    }

    public function getIconColorCode(): ?string
    {
        return $this->iconColorCode;
    }

    public function setIconColorCode(?string $iconColorCode): self
    {
        $this->iconColorCode = $iconColorCode;

        return $this;
    }

    public function getSubjectCategory(): ?TbSubjectCategory
    {
        return $this->subjectCategory;
    }

    public function setSubjectCategory(?TbSubjectCategory $subjectCategory): self
    {
        $this->subjectCategory = $subjectCategory;

        return $this;
    }

    /**
     * @return Collection|TbSubjectSchoolLevel[]
     */
    public function getSubjectSchoolLevel(): Collection
    {
        return $this->subjectSchoolLevel;
    }

    public function addSubjectSchoolLevel(TbSubjectSchoolLevel $subjectSchoolLevel): self
    {
        if (!$this->subjectSchoolLevel->contains($subjectSchoolLevel)) {
            $this->subjectSchoolLevel[] = $subjectSchoolLevel;
            $subjectSchoolLevel->setSubject($this);
        }

        return $this;
    }

    public function removeSubjectSchoolLevel(TbSubjectSchoolLevel $subjectSchoolLevel): self
    {
        if ($this->subjectSchoolLevel->contains($subjectSchoolLevel)) {
            $this->subjectSchoolLevel->removeElement($subjectSchoolLevel);
            // set the owning side to null (unless already changed)
            if ($subjectSchoolLevel->getSubject() === $this) {
                $subjectSchoolLevel->setSubject(null);
            }
        }

        return $this;
    }
}
