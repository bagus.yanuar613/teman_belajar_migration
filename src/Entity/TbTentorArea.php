<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TbTentorArea
 *
 * @ORM\Table(name="tb_tentor_area")
 * @ORM\Entity
 */
class TbTentorArea
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="tentor_id", type="integer", nullable=false)
     */
    private $tentorId;

    /**
     * @var string
     *
     * @ORM\Column(name="kabkota", type="string", length=10, nullable=false)
     */
    private $kabkota;

    /**
     * @var string
     *
     * @ORM\Column(name="kecamatan", type="string", length=15, nullable=false)
     */
    private $kecamatan;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTentorId(): ?int
    {
        return $this->tentorId;
    }

    public function setTentorId(int $tentorId): self
    {
        $this->tentorId = $tentorId;

        return $this;
    }

    public function getKabkota(): ?string
    {
        return $this->kabkota;
    }

    public function setKabkota(string $kabkota): self
    {
        $this->kabkota = $kabkota;

        return $this;
    }

    public function getKecamatan(): ?string
    {
        return $this->kecamatan;
    }

    public function setKecamatan(string $kecamatan): self
    {
        $this->kecamatan = $kecamatan;

        return $this;
    }


}
