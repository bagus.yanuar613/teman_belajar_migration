<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TbAccount
 *
 * @ORM\Table(name="tb_account")
 * @ORM\Entity(repositoryClass="App\Repository\TbAccountRepository")
 */
class TbAccount
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=5, nullable=false)
     */
    private $code;

    /**
     * @var int
     *
     * @ORM\Column(name="type", type="smallint", nullable=false, options={"comment"="tipe akun: 0: admin, 1: tentor, 2: orangtua, 3: murid"})
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=20, nullable=false)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="text", length=65535, nullable=false)
     */
    private $password;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="status", type="boolean", nullable=true, options={"comment"="status pendaftaran: 0: baru, 1: aktif, 2: tidak aktif"})
     */
    private $status;

    /**
     * @var bool
     *
     * @ORM\Column(name="source", type="boolean", nullable=false, options={"comment"="0: website, 1: facebook, 2: google"})
     */
    private $source;

    /**
     * @var string|null
     *
     * @ORM\Column(name="fb_id", type="string", length=100, nullable=true)
     */
    private $fbId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="google_id", type="string", length=100, nullable=true)
     */
    private $googleId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ava", type="text", length=65535, nullable=true)
     */
    private $ava;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified_at", type="datetime", nullable=false)
     */
    private $modifiedAt;


    /**
     * @ORM\OneToOne(targetEntity="App\Entity\TbTentorProfile", mappedBy="account")
     */
    private $tentorProfile;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getStatus(): ?bool
    {
        return $this->status;
    }

    public function setStatus(?bool $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getSource(): ?bool
    {
        return $this->source;
    }

    public function setSource(bool $source): self
    {
        $this->source = $source;

        return $this;
    }

    public function getFbId(): ?string
    {
        return $this->fbId;
    }

    public function setFbId(?string $fbId): self
    {
        $this->fbId = $fbId;

        return $this;
    }

    public function getGoogleId(): ?string
    {
        return $this->googleId;
    }

    public function setGoogleId(?string $googleId): self
    {
        $this->googleId = $googleId;

        return $this;
    }

    public function getAva(): ?string
    {
        return $this->ava;
    }

    public function setAva(?string $ava): self
    {
        $this->ava = $ava;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getModifiedAt(): ?\DateTimeInterface
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt(\DateTimeInterface $modifiedAt): self
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    public function getTentorProfile(): ?TbTentorProfile
    {
        return $this->tentorProfile;
    }

    public function setTentorProfile(?TbTentorProfile $tentorProfile): self
    {
        $this->tentorProfile = $tentorProfile;

        // set (or unset) the owning side of the relation if necessary
        $newAccount = null === $tentorProfile ? null : $this;
        if ($tentorProfile->getAccount() !== $newAccount) {
            $tentorProfile->setAccount($newAccount);
        }

        return $this;
    }
}
