<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TbNotifications
 *
 * @ORM\Table(name="tb_notifications")
 * @ORM\Entity
 */
class TbNotifications
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="user_type", type="boolean", nullable=false, options={"comment"="0: admin, 1: tentor, 2: user"})
     */
    private $userType;

    /**
     * @var int
     *
     * @ORM\Column(name="profile_id", type="integer", nullable=false)
     */
    private $profileId;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=100, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=false)
     */
    private $description;

    /**
     * @var bool
     *
     * @ORM\Column(name="action_type", type="boolean", nullable=false, options={"comment"="1: transactions, 2: payment, 3: poin, 4: profile"})
     */
    private $actionType;

    /**
     * @var string
     *
     * @ORM\Column(name="url_actions", type="string", length=100, nullable=false)
     */
    private $urlActions;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_read", type="boolean", nullable=false, options={"comment"="0: new, 1: read"})
     */
    private $isRead;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datetime", type="datetime", nullable=false)
     */
    private $datetime;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserType(): ?bool
    {
        return $this->userType;
    }

    public function setUserType(bool $userType): self
    {
        $this->userType = $userType;

        return $this;
    }

    public function getProfileId(): ?int
    {
        return $this->profileId;
    }

    public function setProfileId(int $profileId): self
    {
        $this->profileId = $profileId;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getActionType(): ?bool
    {
        return $this->actionType;
    }

    public function setActionType(bool $actionType): self
    {
        $this->actionType = $actionType;

        return $this;
    }

    public function getUrlActions(): ?string
    {
        return $this->urlActions;
    }

    public function setUrlActions(string $urlActions): self
    {
        $this->urlActions = $urlActions;

        return $this;
    }

    public function getIsRead(): ?bool
    {
        return $this->isRead;
    }

    public function setIsRead(bool $isRead): self
    {
        $this->isRead = $isRead;

        return $this;
    }

    public function getDatetime(): ?\DateTimeInterface
    {
        return $this->datetime;
    }

    public function setDatetime(\DateTimeInterface $datetime): self
    {
        $this->datetime = $datetime;

        return $this;
    }


}
