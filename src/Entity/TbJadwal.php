<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TbJadwal
 *
 * @ORM\Table(name="tb_jadwal")
 * @ORM\Entity
 */
class TbJadwal
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="trx_code", type="string", length=15, nullable=false)
     */
    private $trxCode;

    /**
     * @var string
     *
     * @ORM\Column(name="hari", type="string", length=10, nullable=false)
     */
    private $hari;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="jam", type="time", nullable=false)
     */
    private $jam;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_notify", type="boolean", nullable=false)
     */
    private $isNotify;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTrxCode(): ?string
    {
        return $this->trxCode;
    }

    public function setTrxCode(string $trxCode): self
    {
        $this->trxCode = $trxCode;

        return $this;
    }

    public function getHari(): ?string
    {
        return $this->hari;
    }

    public function setHari(string $hari): self
    {
        $this->hari = $hari;

        return $this;
    }

    public function getJam(): ?\DateTimeInterface
    {
        return $this->jam;
    }

    public function setJam(\DateTimeInterface $jam): self
    {
        $this->jam = $jam;

        return $this;
    }

    public function getIsNotify(): ?bool
    {
        return $this->isNotify;
    }

    public function setIsNotify(bool $isNotify): self
    {
        $this->isNotify = $isNotify;

        return $this;
    }


}
