<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TbSettingPoin
 *
 * @ORM\Table(name="tb_setting_poin")
 * @ORM\Entity
 */
class TbSettingPoin
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="batasan", type="boolean", nullable=false)
     */
    private $batasan;

    /**
     * @var int
     *
     * @ORM\Column(name="value", type="integer", nullable=false, options={"comment"="dalam rupiah"})
     */
    private $value;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBatasan(): ?bool
    {
        return $this->batasan;
    }

    public function setBatasan(bool $batasan): self
    {
        $this->batasan = $batasan;

        return $this;
    }

    public function getValue(): ?int
    {
        return $this->value;
    }

    public function setValue(int $value): self
    {
        $this->value = $value;

        return $this;
    }


}
