<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TbUserToken
 *
 * @ORM\Table(name="tb_user_token")
 * @ORM\Entity
 */
class TbUserToken
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    private $userId;

    /**
     * @var bool
     *
     * @ORM\Column(name="type", type="boolean", nullable=false, options={"comment"="0: debit, 1: kredit"})
     */
    private $type;

    /**
     * @var int
     *
     * @ORM\Column(name="value", type="integer", nullable=false)
     */
    private $value;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="tgl_transaksi", type="datetime", nullable=false)
     */
    private $tglTransaksi;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserId(): ?int
    {
        return $this->userId;
    }

    public function setUserId(int $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    public function getType(): ?bool
    {
        return $this->type;
    }

    public function setType(bool $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getValue(): ?int
    {
        return $this->value;
    }

    public function setValue(int $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getTglTransaksi(): ?\DateTimeInterface
    {
        return $this->tglTransaksi;
    }

    public function setTglTransaksi(\DateTimeInterface $tglTransaksi): self
    {
        $this->tglTransaksi = $tglTransaksi;

        return $this;
    }


}
