<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TbSaldoSetting
 *
 * @ORM\Table(name="tb_saldo_setting")
 * @ORM\Entity
 */
class TbSaldoSetting
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="jml_murid", type="integer", nullable=false)
     */
    private $jmlMurid;

    /**
     * @var bool
     *
     * @ORM\Column(name="type", type="boolean", nullable=false, options={"comment"="0: persen, 1: potongan harga"})
     */
    private $type;

    /**
     * @var int
     *
     * @ORM\Column(name="value", type="integer", nullable=false, options={"comment"="potongan untuk teman belajar"})
     */
    private $value;

    /**
     * @var bool
     *
     * @ORM\Column(name="discount_type", type="boolean", nullable=false, options={"comment"="0: persen, 1: potongan harga"})
     */
    private $discountType;

    /**
     * @var int
     *
     * @ORM\Column(name="discount_value", type="integer", nullable=false)
     */
    private $discountValue;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified_at", type="datetime", nullable=false)
     */
    private $modifiedAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getJmlMurid(): ?int
    {
        return $this->jmlMurid;
    }

    public function setJmlMurid(int $jmlMurid): self
    {
        $this->jmlMurid = $jmlMurid;

        return $this;
    }

    public function getType(): ?bool
    {
        return $this->type;
    }

    public function setType(bool $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getValue(): ?int
    {
        return $this->value;
    }

    public function setValue(int $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getDiscountType(): ?bool
    {
        return $this->discountType;
    }

    public function setDiscountType(bool $discountType): self
    {
        $this->discountType = $discountType;

        return $this;
    }

    public function getDiscountValue(): ?int
    {
        return $this->discountValue;
    }

    public function setDiscountValue(int $discountValue): self
    {
        $this->discountValue = $discountValue;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getModifiedAt(): ?\DateTimeInterface
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt(\DateTimeInterface $modifiedAt): self
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }


}
