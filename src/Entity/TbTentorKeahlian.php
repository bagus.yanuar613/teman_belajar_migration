<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TbTentorKeahlian
 *
 * @ORM\Table(name="tb_tentor_keahlian", indexes={@ORM\Index(name="subjectschoollevel_id", columns={"subjectschoollevel_id"}), @ORM\Index(name="tentor_id", columns={"tentor_id"})})
 * @ORM\Entity(repositoryClass="App\Repository\TbTentorKeahlianRepository")
 */
class TbTentorKeahlian
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="tarif", type="integer", nullable=false)
     */
    private $tarif;

    /**
     * @var bool
     *
     * @ORM\Column(name="has_parent", type="boolean", nullable=false, options={"comment"="1: active, 0: not_active"})
     */
    private $hasParent;

    /**
     * @var \TbSubjectSchoolLevel
     *
     * @ORM\ManyToOne(targetEntity="TbSubjectSchoolLevel")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="subjectschoollevel_id", referencedColumnName="id")
     * })
     */
    private $subjectschoollevel;

    /**
     * @var \TbTentorProfile
     *
     * @ORM\ManyToOne(targetEntity="TbTentorProfile", inversedBy="keahlian")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tentor_id", referencedColumnName="id")
     * })
     */
    private $tentor;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTarif(): ?int
    {
        return $this->tarif;
    }

    public function setTarif(int $tarif): self
    {
        $this->tarif = $tarif;

        return $this;
    }

    public function getHasParent(): ?bool
    {
        return $this->hasParent;
    }

    public function setHasParent(bool $hasParent): self
    {
        $this->hasParent = $hasParent;

        return $this;
    }

    public function getSubjectschoollevel(): ?TbSubjectSchoolLevel
    {
        return $this->subjectschoollevel;
    }

    public function setSubjectschoollevel(?TbSubjectSchoolLevel $subjectschoollevel): self
    {
        $this->subjectschoollevel = $subjectschoollevel;

        return $this;
    }

    public function getTentor(): ?TbTentorProfile
    {
        return $this->tentor;
    }

    public function setTentor(?TbTentorProfile $tentor): self
    {
        $this->tentor = $tentor;

        return $this;
    }


}
