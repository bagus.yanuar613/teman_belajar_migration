<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TbTentorFavorit
 *
 * @ORM\Table(name="tb_tentor_favorit")
 * @ORM\Entity
 */
class TbTentorFavorit
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="tentor_id", type="integer", nullable=false)
     */
    private $tentorId;

    /**
     * @var int
     *
     * @ORM\Column(name="user_account_id", type="integer", nullable=false)
     */
    private $userAccountId;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTentorId(): ?int
    {
        return $this->tentorId;
    }

    public function setTentorId(int $tentorId): self
    {
        $this->tentorId = $tentorId;

        return $this;
    }

    public function getUserAccountId(): ?int
    {
        return $this->userAccountId;
    }

    public function setUserAccountId(int $userAccountId): self
    {
        $this->userAccountId = $userAccountId;

        return $this;
    }


}
