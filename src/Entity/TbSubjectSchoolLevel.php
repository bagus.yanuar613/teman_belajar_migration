<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TbSubjectSchoolLevel
 *
 * @ORM\Table(name="tb_subject_school_level", indexes={@ORM\Index(name="school_level_id", columns={"school_level_id"}), @ORM\Index(name="subject_id", columns={"subject_id"})})
 * @ORM\Entity(repositoryClass="App\Repository\TbSubjectSchoolLevelRepository")
 */
class TbSubjectSchoolLevel
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \TbSchoolLevel
     *
     * @ORM\ManyToOne(targetEntity="TbSchoolLevel", inversedBy="subjectSchoolLevel")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="school_level_id", referencedColumnName="id")
     * })
     */
    private $schoolLevel;

    /**
     * @var \TbSubjects
     *
     * @ORM\ManyToOne(targetEntity="TbSubjects", inversedBy="subjectSchoolLevel")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="subject_id", referencedColumnName="id")
     * })
     */
    private $subject;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSchoolLevel(): ?TbSchoolLevel
    {
        return $this->schoolLevel;
    }

    public function setSchoolLevel(?TbSchoolLevel $schoolLevel): self
    {
        $this->schoolLevel = $schoolLevel;

        return $this;
    }

    public function getSubject(): ?TbSubjects
    {
        return $this->subject;
    }

    public function setSubject(?TbSubjects $subject): self
    {
        $this->subject = $subject;

        return $this;
    }


}
