<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TbSaldoTentor
 *
 * @ORM\Table(name="tb_saldo_tentor")
 * @ORM\Entity
 */
class TbSaldoTentor
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="type", type="boolean", nullable=false, options={"comment"="0: debit, 1: kredit"})
     */
    private $type;

    /**
     * @var int
     *
     * @ORM\Column(name="tentor_id", type="integer", nullable=false)
     */
    private $tentorId;

    /**
     * @var int
     *
     * @ORM\Column(name="value", type="integer", nullable=false)
     */
    private $value;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="tgl_transaksi", type="datetime", nullable=false)
     */
    private $tglTransaksi;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?bool
    {
        return $this->type;
    }

    public function setType(bool $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getTentorId(): ?int
    {
        return $this->tentorId;
    }

    public function setTentorId(int $tentorId): self
    {
        $this->tentorId = $tentorId;

        return $this;
    }

    public function getValue(): ?int
    {
        return $this->value;
    }

    public function setValue(int $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getTglTransaksi(): ?\DateTimeInterface
    {
        return $this->tglTransaksi;
    }

    public function setTglTransaksi(\DateTimeInterface $tglTransaksi): self
    {
        $this->tglTransaksi = $tglTransaksi;

        return $this;
    }


}
