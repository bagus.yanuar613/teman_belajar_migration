<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * TbSchoolLevel
 *
 * @ORM\Table(name="tb_school_level")
 * @ORM\Entity(repositoryClass="App\Repository\TbSchoolLevelRepository")
 */
class TbSchoolLevel
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="permalink", type="string", length=255, nullable=false)
     */
    private $permalink;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TbSubjectSchoolLevel", mappedBy="schoolLevel")
     */
    private $subjectSchoolLevel;

    public function __construct()
    {
        $this->subjectSchoolLevel = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPermalink(): ?string
    {
        return $this->permalink;
    }

    public function setPermalink(string $permalink): self
    {
        $this->permalink = $permalink;

        return $this;
    }

    /**
     * @return Collection|TbSubjectSchoolLevel[]
     */
    public function getSubjectSchoolLevel(): Collection
    {
        return $this->subjectSchoolLevel;
    }

    public function addSubjectSchoolLevel(TbSubjectSchoolLevel $subjectSchoolLevel): self
    {
        if (!$this->subjectSchoolLevel->contains($subjectSchoolLevel)) {
            $this->subjectSchoolLevel[] = $subjectSchoolLevel;
            $subjectSchoolLevel->setSchoolLevel($this);
        }

        return $this;
    }

    public function removeSubjectSchoolLevel(TbSubjectSchoolLevel $subjectSchoolLevel): self
    {
        if ($this->subjectSchoolLevel->contains($subjectSchoolLevel)) {
            $this->subjectSchoolLevel->removeElement($subjectSchoolLevel);
            // set the owning side to null (unless already changed)
            if ($subjectSchoolLevel->getSchoolLevel() === $this) {
                $subjectSchoolLevel->setSchoolLevel(null);
            }
        }

        return $this;
    }

}
