<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TbUserPayment
 *
 * @ORM\Table(name="tb_user_payment")
 * @ORM\Entity
 */
class TbUserPayment
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="trx_code", type="string", length=15, nullable=false)
     */
    private $trxCode;

    /**
     * @var int
     *
     * @ORM\Column(name="bank_id", type="integer", nullable=false)
     */
    private $bankId;

    /**
     * @var string
     *
     * @ORM\Column(name="account_name", type="string", length=50, nullable=false)
     */
    private $accountName;

    /**
     * @var string
     *
     * @ORM\Column(name="account_number", type="string", length=50, nullable=false)
     */
    private $accountNumber;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="payment_datetime", type="datetime", nullable=false)
     */
    private $paymentDatetime;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTrxCode(): ?string
    {
        return $this->trxCode;
    }

    public function setTrxCode(string $trxCode): self
    {
        $this->trxCode = $trxCode;

        return $this;
    }

    public function getBankId(): ?int
    {
        return $this->bankId;
    }

    public function setBankId(int $bankId): self
    {
        $this->bankId = $bankId;

        return $this;
    }

    public function getAccountName(): ?string
    {
        return $this->accountName;
    }

    public function setAccountName(string $accountName): self
    {
        $this->accountName = $accountName;

        return $this;
    }

    public function getAccountNumber(): ?string
    {
        return $this->accountNumber;
    }

    public function setAccountNumber(string $accountNumber): self
    {
        $this->accountNumber = $accountNumber;

        return $this;
    }

    public function getPaymentDatetime(): ?\DateTimeInterface
    {
        return $this->paymentDatetime;
    }

    public function setPaymentDatetime(\DateTimeInterface $paymentDatetime): self
    {
        $this->paymentDatetime = $paymentDatetime;

        return $this;
    }


}
