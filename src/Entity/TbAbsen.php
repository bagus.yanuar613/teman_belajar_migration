<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TbAbsen
 *
 * @ORM\Table(name="tb_absen")
 * @ORM\Entity
 */
class TbAbsen
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="trx_code", type="string", length=15, nullable=false)
     */
    private $trxCode;

    /**
     * @var string
     *
     * @ORM\Column(name="day", type="string", length=10, nullable=false)
     */
    private $day;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datetime", type="datetime", nullable=false)
     */
    private $datetime;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="presence", type="boolean", nullable=true, options={"comment"="0: absen, 1: hadir"})
     */
    private $presence;

    /**
     * @var bool
     *
     * @ORM\Column(name="status", type="boolean", nullable=false, options={"comment"="0: absen biasa, 1: pertemuan pertama	"})
     */
    private $status;

    /**
     * @var string|null
     *
     * @ORM\Column(name="is_tombol", type="string", length=5, nullable=true)
     */
    private $isTombol;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTrxCode(): ?string
    {
        return $this->trxCode;
    }

    public function setTrxCode(string $trxCode): self
    {
        $this->trxCode = $trxCode;

        return $this;
    }

    public function getDay(): ?string
    {
        return $this->day;
    }

    public function setDay(string $day): self
    {
        $this->day = $day;

        return $this;
    }

    public function getDatetime(): ?\DateTimeInterface
    {
        return $this->datetime;
    }

    public function setDatetime(\DateTimeInterface $datetime): self
    {
        $this->datetime = $datetime;

        return $this;
    }

    public function getPresence(): ?bool
    {
        return $this->presence;
    }

    public function setPresence(?bool $presence): self
    {
        $this->presence = $presence;

        return $this;
    }

    public function getStatus(): ?bool
    {
        return $this->status;
    }

    public function setStatus(bool $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getIsTombol(): ?string
    {
        return $this->isTombol;
    }

    public function setIsTombol(?string $isTombol): self
    {
        $this->isTombol = $isTombol;

        return $this;
    }


}
