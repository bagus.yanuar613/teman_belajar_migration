<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * TbTentorProfile
 *
 * @ORM\Table(name="tb_tentor_profile", indexes={@ORM\Index(name="account_id", columns={"account_id"})})
 * @ORM\Entity(repositoryClass="App\Repository\TbTentorProfileRepository")
 */
class TbTentorProfile
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=5, nullable=false)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="fullname", type="string", length=100, nullable=false)
     */
    private $fullname;

    /**
     * @var string
     *
     * @ORM\Column(name="permalink", type="string", length=255, nullable=false)
     */
    private $permalink;

    /**
     * @var string
     *
     * @ORM\Column(name="gender", type="string", length=20, nullable=false)
     */
    private $gender;

    /**
     * @var string
     *
     * @ORM\Column(name="about_me", type="text", length=65535, nullable=false)
     */
    private $aboutMe;

    /**
     * @var string
     *
     * @ORM\Column(name="experience", type="text", length=65535, nullable=false)
     */
    private $experience;

    /**
     * @var string
     *
     * @ORM\Column(name="education", type="string", length=100, nullable=false)
     */
    private $education;

    /**
     * @var string
     *
     * @ORM\Column(name="achievement", type="string", length=100, nullable=false)
     */
    private $achievement;

    /**
     * @var string
     *
     * @ORM\Column(name="work_at", type="string", length=100, nullable=false)
     */
    private $workAt;

    /**
     * @var bool
     *
     * @ORM\Column(name="status", type="boolean", nullable=false, options={"comment"="1: active, 0: not_active"})
     */
    private $status;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="is_verified", type="boolean", nullable=true, options={"comment"="1: verified, 0: not verified"})
     */
    private $isVerified;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified_at", type="datetime", nullable=false)
     */
    private $modifiedAt;

    /**
     * @var bool
     *
     * @ORM\Column(name="has_parent", type="boolean", nullable=false, options={"comment"="1: active, 0: not_active"})
     */
    private $hasParent;

    /**
     * @var \TbAccount
     *
     * @ORM\ManyToOne(targetEntity="TbAccount", inversedBy="tentorProfile")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="account_id", referencedColumnName="id")
     * })
     */
    private $account;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TbTentorKeahlian", mappedBy="tentor")
     */
    private $keahlian;

    public function __construct()
    {
        $this->keahlian = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getFullname(): ?string
    {
        return $this->fullname;
    }

    public function setFullname(string $fullname): self
    {
        $this->fullname = $fullname;

        return $this;
    }

    public function getPermalink(): ?string
    {
        return $this->permalink;
    }

    public function setPermalink(string $permalink): self
    {
        $this->permalink = $permalink;

        return $this;
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function setGender(string $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    public function getAboutMe(): ?string
    {
        return $this->aboutMe;
    }

    public function setAboutMe(string $aboutMe): self
    {
        $this->aboutMe = $aboutMe;

        return $this;
    }

    public function getExperience(): ?string
    {
        return $this->experience;
    }

    public function setExperience(string $experience): self
    {
        $this->experience = $experience;

        return $this;
    }

    public function getEducation(): ?string
    {
        return $this->education;
    }

    public function setEducation(string $education): self
    {
        $this->education = $education;

        return $this;
    }

    public function getAchievement(): ?string
    {
        return $this->achievement;
    }

    public function setAchievement(string $achievement): self
    {
        $this->achievement = $achievement;

        return $this;
    }

    public function getWorkAt(): ?string
    {
        return $this->workAt;
    }

    public function setWorkAt(string $workAt): self
    {
        $this->workAt = $workAt;

        return $this;
    }

    public function getStatus(): ?bool
    {
        return $this->status;
    }

    public function setStatus(bool $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getIsVerified(): ?bool
    {
        return $this->isVerified;
    }

    public function setIsVerified(?bool $isVerified): self
    {
        $this->isVerified = $isVerified;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getModifiedAt(): ?\DateTimeInterface
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt(\DateTimeInterface $modifiedAt): self
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    public function getHasParent(): ?bool
    {
        return $this->hasParent;
    }

    public function setHasParent(bool $hasParent): self
    {
        $this->hasParent = $hasParent;

        return $this;
    }

    public function getAccount(): ?TbAccount
    {
        return $this->account;
    }

    public function setAccount(?TbAccount $account): self
    {
        $this->account = $account;

        return $this;
    }

    /**
     * @return Collection|TbTentorKeahlian[]
     */
    public function getKeahlian(): Collection
    {
        return $this->keahlian;
    }

    public function addKeahlian(TbTentorKeahlian $keahlian): self
    {
        if (!$this->keahlian->contains($keahlian)) {
            $this->keahlian[] = $keahlian;
            $keahlian->setTentor($this);
        }

        return $this;
    }

    public function removeKeahlian(TbTentorKeahlian $keahlian): self
    {
        if ($this->keahlian->contains($keahlian)) {
            $this->keahlian->removeElement($keahlian);
            // set the owning side to null (unless already changed)
            if ($keahlian->getTentor() === $this) {
                $keahlian->setTentor(null);
            }
        }

        return $this;
    }
}
