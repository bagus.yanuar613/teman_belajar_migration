<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TbTentorUlasan
 *
 * @ORM\Table(name="tb_tentor_ulasan")
 * @ORM\Entity
 */
class TbTentorUlasan
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="trx_code", type="string", length=15, nullable=false)
     */
    private $trxCode;

    /**
     * @var bool
     *
     * @ORM\Column(name="rating", type="boolean", nullable=false)
     */
    private $rating;

    /**
     * @var string
     *
     * @ORM\Column(name="ulasan", type="text", length=65535, nullable=false)
     */
    private $ulasan;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified_at", type="datetime", nullable=false)
     */
    private $modifiedAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTrxCode(): ?string
    {
        return $this->trxCode;
    }

    public function setTrxCode(string $trxCode): self
    {
        $this->trxCode = $trxCode;

        return $this;
    }

    public function getRating(): ?bool
    {
        return $this->rating;
    }

    public function setRating(bool $rating): self
    {
        $this->rating = $rating;

        return $this;
    }

    public function getUlasan(): ?string
    {
        return $this->ulasan;
    }

    public function setUlasan(string $ulasan): self
    {
        $this->ulasan = $ulasan;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getModifiedAt(): ?\DateTimeInterface
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt(\DateTimeInterface $modifiedAt): self
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }


}
